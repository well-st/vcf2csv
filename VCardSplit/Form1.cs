﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VCardSplit
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "vcard|*.vcf";
            if (DialogResult.OK == openFileDialog1.ShowDialog())
            {
                string filename = openFileDialog1.FileName;
                List<vcard> lst = new List<vcard>();
                string path = folderBrowserDialog1.SelectedPath;
                StreamReader sr = new StreamReader(filename);
                StreamWriter sw = new StreamWriter(filename.Replace(".vcf", ".csv"));
                vcard v = new vcard();
                sw.WriteLine("姓名,手机,邮箱");
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (line == "BEGIN:VCARD")
                    {
                        v = new vcard();
                        lst.Add(v);
                    }
                    if (line.StartsWith("N"))
                    {
                        v.name = getname(line);
                    }
                    if (line.StartsWith("TEL"))
                    {
                        v.mobile = getmobile(line);
                    }
                    if (line.StartsWith("EMAIL"))
                    {
                        v.email = line.Replace("EMAIL:", "");
                    }
                    if (line == "END:VCARD")
                    {
                        writevcard(v, sw);
                    }
                }
                echolist(lst);
                sr.Close();
                sr.Dispose();
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
            MessageBox.Show("OK");
        }
        private void writevcard(vcard v, StreamWriter sw)
        {
            sw.WriteLine(string.Format("{0},{1},{2}", v.name, v.mobile, v.email));
        }
        private void echolist(List<vcard> lst)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("姓名,手机,邮箱\r\n");
            foreach (vcard v in lst)
            {
                sb.Append(string.Format("{0},{1},{2}\r\n", v.name, v.mobile, v.email));
            }
            richTextBox1.Text = sb.ToString();
        }
        private string getmobile(string line)
        {
            string[] ps = line.Split(':');
            return ps[1].Replace("-", "");
        }
        private string getname(string line)
        {
            string[] ps = line.Split(';');
            if (ps[1] == "CHARSET=UTF-8")
            {
                //=E5=86=AF=E6=AF=85=E5=BD=AC
                string utf8name = ps[3];
                return utf8codeToHZ(utf8name);
            }
            else
            {
                return ps[1];
            }
        }
        private string utf8codeToHZ(string utf8code)
        {
            string[] b6 = utf8code.Split('=');
            string name = "";
            List<byte> lb = new List<byte>();
            foreach (string b in b6)
            {
                if (!string.IsNullOrWhiteSpace(b))
                {
                    lb.Add((byte)Convert.ToByte(b, 16));
                }
            }
            name += Encoding.GetEncoding("UTF-8").GetString(lb.ToArray());
            return name;
        }
    }
    public class vcard
    {
        public string name { set; get; }
        public string mobile { set; get; }
        public string email { set; get; }
        public string mobile2 { set; get; }
    }
}
